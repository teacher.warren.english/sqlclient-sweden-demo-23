﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientSweden23
{
    internal interface IArtistRepository
    {
        /// <summary>
        /// Add a new artist to the db.
        /// </summary>
        /// <param name="newArtist">The new artist object to be added.</param>
        /// <returns>True if the new artist has been successfully added to the db.</returns>
        bool AddArtist(Artist newArtist);
        IList<Artist> GetAllArtists();
        void DeleteArtist(Artist newArtist);
        void UpdateArtist(Artist newArtist);

    }
}
