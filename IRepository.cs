﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientSweden23
{
    internal interface IRepository<T>
    {
        IList<T> GetAll();
        bool AddNewEntity(T newEntity);
    }
}
