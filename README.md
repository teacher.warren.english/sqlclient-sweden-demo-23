# SqlClient Demo
A demonstration of the SqlClient package by Microsoft, to connect your console application to your SqlServer database, and execute SQL commands on it. Also a simple implementation of the repository pattern. A class demo from the Sweden 2023 full stack bootcamp.


## Installation

To clone this repository, run the following command:

```bash
git clone https://gitlab.com/teacher.warren.english/sqlclient-sweden-demo-23.git
```

## Contributing

@teacher.warren.english

## License
©️ Noroff Accelerate
[MIT](https://choosealicense.com/licenses/mit/)
