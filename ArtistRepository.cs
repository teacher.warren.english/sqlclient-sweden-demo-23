﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientSweden23
{
    internal class ArtistRepository : IArtistRepository
    {
        public bool AddArtist(Artist newArtist)
        {
            using SqlConnection connection = new(DbConfig.GetConnectionString());

            connection.Open();

            // NB! Insert variable elements into SQL command using SqlCommand.Parameters.AddWithValue()
            string sql = "INSERT INTO Artist (ArtistName, ArtistGenre, RecordLabelId) VALUES (@name, @genre, @rlid)";

            SqlCommand cmd = new(sql, connection);
            cmd.Parameters.AddWithValue("@name", newArtist.Name);
            cmd.Parameters.AddWithValue("@genre", newArtist.Genre);
            cmd.Parameters.AddWithValue("@rlid", newArtist.RecordLabelId);

            return cmd.ExecuteNonQuery() == 1;
        }

        public void DeleteArtist(Artist newArtist)
        {
            throw new NotImplementedException();
        }

        public IList<Artist> GetAllArtists()
        {
            using SqlConnection connection = new(DbConfig.GetConnectionString()); // "Data Source=localhost\SQLEXPRESS;Initial Catalog=MusicEmpire;Integrated Security=True"
            connection.Open();

            string sql = "SELECT * FROM Artist";

            using SqlCommand cmd = new(sql, connection);

            IList<Artist> artists = new List<Artist>();

            using SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Artist artist = new();

                artist.Id = reader.GetInt32(0);
                artist.Name = reader.GetString(1);
                artist.Genre = reader.GetString(2);
                artist.RecordLabelId = reader.GetInt32(3);

                artists.Add(artist);
            }

            return artists;
        }

        public void UpdateArtist(Artist newArtist)
        {
            throw new NotImplementedException();
        }
    }
}
