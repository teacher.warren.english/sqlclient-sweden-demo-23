﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientSweden23
{
    internal class DbConfig
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new()
            {
                DataSource = @"localhost\SQLEXPRESS",
                InitialCatalog = "MusicEmpire",
                IntegratedSecurity = true
            };

            return builder.ConnectionString;
        }
    }
}
