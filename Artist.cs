﻿// Install dependency ✔️
// Create Connection ✔️
// Open Connection ✔️
// Define SQL Command
// Execute SQL
// Process results

internal class Artist
{
    // PK
    public int Id { get; set; }
    public string Name { get; set; }
    public string Genre { get; set; }
    // FK
    public int RecordLabelId { get; set; }
}