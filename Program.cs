﻿// Install dependency ✔️
// Create Connection ✔️
// Open Connection ✔️
// Define SQL Command ✔️
// Execute SQL ✔️
// Process results ✔️

using SqlClientSweden23;

IArtistRepository repo = new ArtistRepository();

// Static Void Main Logic

IEnumerable<Artist> artists = repo.GetAllArtists();

foreach (var a in artists)
    Console.WriteLine($"{a.Id}\t{a.Name}\t{a.Genre}\t{a.RecordLabelId}");


// Add artist
//Artist niki = new()
//{
//    Name = "Niki Minaj",
//    Genre = "Hiphop",
//    RecordLabelId = 2,
//};

//repo.AddArtist(niki);